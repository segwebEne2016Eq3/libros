package control;

import java.io.IOException;
import java.io.Serializable;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@Named("loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    private String userName;
    private String userPwd;

    public LoginBean() {
    }

    public String getUserName() {
        System.out.println("plip plaut");
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public String login() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext externalContext = context.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        String pajina = null;
        try {
            request.login(userName, userPwd);
            
            if (request.isUserInRole("Administrador")) {
                System.out.println("Hip");
                pajina = "/Administrador/Administrador";
            } else if (request.isUserInRole("Usuario")) {
                pajina = "/Usuario/Usuario";
            } else if (request.isUserInRole("Bibliotecario")) {
                pajina = "/Bibliotecario/Bibliotecario";
            } else {
                context.addMessage(null, new FacesMessage("Credenciales insuficientes"));
                pajina = "welcomePrimefaces";
            }
        } catch (ServletException e) {
         
          
            
            pajina = "/Usuario/Create";
            context.addMessage(null, new FacesMessage("Error en la convinación usuario/contraseña(Registrarse)"));
            
        }
        return pajina;
    }


    public void logout() throws IOException {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        ec.invalidateSession();
        ec.redirect(ec.getRequestContextPath() + "/faces/index.xhtml");
    }

  
}
