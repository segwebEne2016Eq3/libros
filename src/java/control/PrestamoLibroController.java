package control;

import entidad.AutorLibro;
import entidad.Libro;
import entidad.Reservacion;
import sesion.AutorLibroFacade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.servlet.http.HttpServletRequest;

@Named("prestamoLibroController")
@SessionScoped
public class PrestamoLibroController implements Serializable {

    @EJB
    private sesion.AutorLibroFacade ejbFacade;
    @EJB
    private sesion.LibroFacade libroFacade;
    @EJB
    private sesion.ReservacionFacade reservacionFacade;
    @EJB
    private sesion.UsuarioFacade usuarioFacade;
    private List<AutorLibro> items = null;
    private AutorLibro selected;
    private Libro libroSeleccionado;
    private Reservacion reservacionSeleccionada;
    private String indicio;
    private List<Libro> agregados = new ArrayList();
    private String buscarPor;
    private String userName = "oscar";
    private Date fechaRecogerLibros;
    private List<Reservacion> reservaciones = null;

    public PrestamoLibroController() {
    }
    
    public Libro getLibroSeleccionado() {
        return libroSeleccionado;
    }

    public void setLibroSeleccionado(Libro libroSeleccionado) {
        this.libroSeleccionado = libroSeleccionado;
    }

    public Reservacion getReservacionSeleccionada() {
        return reservacionSeleccionada;
    }

    public void setReservacionSeleccionada(Reservacion reservacionSeleccionada) {
        this.reservacionSeleccionada = reservacionSeleccionada;
    }
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String usuario) {
        this.userName = usuario;
    }

    public void loadNew() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        //userName = System.out.println(request.getUserPrincipal().getName());
        List<Reservacion> res = reservacionFacade.findDate("atrasadas");
        for (Reservacion r: res)
            if (r.getIdLibro() != null) {
                int cantidad  = r.getIdLibro().getCantidadEjemplares();
                r.getIdLibro().setCantidadEjemplares(cantidad + 1);
                r.setIdLibro(null);
                reservacionFacade.edit(r);
            }
    }

    public AutorLibro getSelected() {
        return selected;
    }

    public void setSelected(AutorLibro selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private AutorLibroFacade getFacade() {
        return ejbFacade;
    }

    public String getIndicio() {
        return indicio;
    }

    public void setIndicio(String indicio) {
        this.indicio = indicio;
    }

    public void buscar() {
        if (buscarPor != null) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void buscarReservaciones() {
        if (buscarPor != null) {
            reservaciones = null;    // Invalidate list of items to trigger re-query.
        }
    }
    
    public List<AutorLibro> getItems() {
        if (items == null) {
            if (indicio != null) {  
                items = getFacade().find(buscarPor, indicio);
                indicio = null;
            } else {
                items = getFacade().findDistinctLibro();
            }
        }
        return items;
    }

    public List<Reservacion> getReservaciones() {
        if (reservaciones == null) {
            if (buscarPor != null)
                reservaciones = reservacionFacade.findDate(buscarPor, userName);
            else
                reservaciones = reservacionFacade.findDate("cualquier fecha", userName);
        }
        return reservaciones;
    }
    
    
    
    public String getBuscarPor() {
        return buscarPor;
    }

    public void setBuscarPor(String buscarPor) {
        this.buscarPor = buscarPor;
    }

    public List<Libro> getAgregados() {
        return agregados;
    }

    public void eliminarAgregado() {
        agregados.remove(libroSeleccionado);
    }
    
    public void eliminarReservacion() {
        if (reservacionSeleccionada != null) {
            Libro l = reservacionSeleccionada.getIdLibro();
            int candidat = l.getCantidadEjemplares();
            l.setCantidadEjemplares(candidat + 1);
            libroFacade.edit(l);
            reservacionFacade.remove(reservacionSeleccionada);
            reservaciones = null;
            items = null;
            indicio = null;
        }
    }

    public Date getFechaRecogerLibros() {
        return fechaRecogerLibros;
    }

    public void setFechaRecogerLibros(Date fechaRecogerLibros) {
        this.fechaRecogerLibros = fechaRecogerLibros;
    }
    
    public void guardarReservacion() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (fechaRecogerLibros != null) {
            int count = agregados.size();
            if (!agregados.isEmpty()) {
                for (Libro l: agregados) {
                    int cantidad = l.getCantidadEjemplares();
                    l.setCantidadEjemplares(cantidad - 1);
                    libroFacade.edit(l);
                    Reservacion r = new Reservacion();
                    r.setIdLibro(l);
                    r.setFechaRecogerLibros(fechaRecogerLibros);
                    r.setFechaReservacion(new Date());
                    r.setIdUsuario(usuarioFacade.findUser(userName));
                    reservacionFacade.create(r);
                }
                fechaRecogerLibros = null;
                reservaciones = null;
                agregados = new ArrayList();
                context.addMessage(null, new FacesMessage("Has reservado " + count + " libros"));
            } else
                context.addMessage(null, new FacesMessage("No has agregado libros"));
        } else
            context.addMessage(null, new FacesMessage("Debes especificar la fecha para recoger libros"));
    }
    
    public void agregar() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (selected.getIdLibro().getCantidadEjemplares() <= 0)
            context.addMessage(null, new FacesMessage("No hay Ejemplares disponibles de este libro"));
        else if (existeReservacion(selected.getIdLibro()))
            context.addMessage(null, new FacesMessage("Ya tienes reservado el libro \"" + selected.getIdLibro().getTitulo() + "\""));
        else if (!agregados.contains(selected.getIdLibro())) {
            agregados.add(selected.getIdLibro());
            items.remove(selected);
        } else
            context.addMessage(null, new FacesMessage("No puedes agregar 2 veces el mismo libro"));
    }

    private boolean existeReservacion(Libro l) {
        List<Reservacion> res = reservacionFacade.findDate("cualquier fecha", userName);
        for (Reservacion r: res) {
            if (r.getIdLibro() != null) {
                if ((int) r.getIdLibro().getIdLibro() == (int) l.getIdLibro())
                    return true;
            }
        }
        return false;
    }
    
    public AutorLibro getAutorLibro(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<AutorLibro> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<AutorLibro> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = AutorLibro.class)
    public static class AutorLibroControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            PrestamoLibroController controller = (PrestamoLibroController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "autorLibroController");
            return controller.getAutorLibro(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof AutorLibro) {
                AutorLibro o = (AutorLibro) object;
                return getStringKey(o.getIdAutorLibro());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), AutorLibro.class.getName()});
                return null;
            }
        }

    }

    public Date getMinDate() {
        return new Date();
    }
    
    public Date getMaxDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_YEAR, 10);
        return calendar.getTime();
    }
    
}
