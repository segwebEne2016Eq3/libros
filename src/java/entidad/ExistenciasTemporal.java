/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entidad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author susi
 */
@Entity
@Table(name = "existencias_temporal")
@NamedQueries({
    @NamedQuery(name = "ExistenciasTemporal.findAll", query = "SELECT e FROM ExistenciasTemporal e")})
public class ExistenciasTemporal implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_existencias")
    private Integer idExistencias;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidad_ejemplares")
    private int cantidadEjemplares;
    @JoinColumn(name = "id_libro", referencedColumnName = "id_libro")
    @ManyToOne(optional = false)
    private Libro idLibro;

    public ExistenciasTemporal() {
    }

    public ExistenciasTemporal(Integer idExistencias) {
        this.idExistencias = idExistencias;
    }

    public ExistenciasTemporal(Integer idExistencias, int cantidadEjemplares) {
        this.idExistencias = idExistencias;
        this.cantidadEjemplares = cantidadEjemplares;
    }

    public Integer getIdExistencias() {
        return idExistencias;
    }

    public void setIdExistencias(Integer idExistencias) {
        this.idExistencias = idExistencias;
    }

    public int getCantidadEjemplares() {
        return cantidadEjemplares;
    }

    public void setCantidadEjemplares(int cantidadEjemplares) {
        this.cantidadEjemplares = cantidadEjemplares;
    }

    public Libro getIdLibro() {
        return idLibro;
    }

    public void setIdLibro(Libro idLibro) {
        this.idLibro = idLibro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idExistencias != null ? idExistencias.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExistenciasTemporal)) {
            return false;
        }
        ExistenciasTemporal other = (ExistenciasTemporal) object;
        if ((this.idExistencias == null && other.idExistencias != null) || (this.idExistencias != null && !this.idExistencias.equals(other.idExistencias))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.ExistenciasTemporal[ idExistencias=" + idExistencias + " ]";
    }
    
}
