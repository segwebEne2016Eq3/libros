/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entidad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author susi
 */
@Entity
@Table(name = "autor_libro")
@NamedQueries({
    @NamedQuery(name = "AutorLibro.findAll", query = "SELECT a FROM AutorLibro a")})
public class AutorLibro implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_autor_libro")
    private Integer idAutorLibro;
    @JoinColumn(name = "id_libro", referencedColumnName = "id_libro")
    @ManyToOne(optional = false)
    private Libro idLibro;
    @JoinColumn(name = "id_autor", referencedColumnName = "id_autor")
    @ManyToOne(optional = false)
    private Autor idAutor;

    public AutorLibro() {
    }

    public AutorLibro(Integer idAutorLibro) {
        this.idAutorLibro = idAutorLibro;
    }

    public Integer getIdAutorLibro() {
        return idAutorLibro;
    }

    public void setIdAutorLibro(Integer idAutorLibro) {
        this.idAutorLibro = idAutorLibro;
    }

    public Libro getIdLibro() {
        return idLibro;
    }

    public void setIdLibro(Libro idLibro) {
        this.idLibro = idLibro;
    }

    public Autor getIdAutor() {
        return idAutor;
    }

    public void setIdAutor(Autor idAutor) {
        this.idAutor = idAutor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAutorLibro != null ? idAutorLibro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AutorLibro)) {
            return false;
        }
        AutorLibro other = (AutorLibro) object;
        if ((this.idAutorLibro == null && other.idAutorLibro != null) || (this.idAutorLibro != null && !this.idAutorLibro.equals(other.idAutorLibro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.AutorLibro[ idAutorLibro=" + idAutorLibro + " ]";
    }
    
}
