/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import java.util.List;
import javax.persistence.EntityManager;
/**
 *
 * @author torpre
 */
public abstract class AbstractFacade<T> {

    private Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();

    public void create(T entity) {
        getEntityManager().persist(entity);
    }

    public void edit(T entity) {
        getEntityManager().merge(entity);
    }

    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }
    
    public T findUser(String user) {
        javax.persistence.criteria.CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = cb.createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(rt);
        cq.where(cb.like(rt.get("usuario"), user));
        return (T) getEntityManager().createQuery(cq).getSingleResult();
    }
    
    public T findEjemplar(String barcode) {
        javax.persistence.criteria.CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = cb.createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(rt);
        cq.where(cb.like(rt.get("idEjemplar"), barcode));
        return (T) getEntityManager().createQuery(cq).getSingleResult();
    }
    
    /**
     * 
     * @param buscarPor sirve para buscar por autor, titulo, isbn, categoría, subcategoría
     * @param indicio indicio de busquedá, no es necesario una palabra correcta, ejemplo: "BlanCa Nie"
     * @return 
     */
    public List<T> find(String buscarPor, String indicio) {
        indicio = "%" + indicio.trim().toUpperCase() + "%";
        javax.persistence.criteria.CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = cb.createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(rt);
        if (buscarPor.compareTo("autor") == 0) {
            cq.where(cb.like(cb.upper(rt.get("idAutor").get("nombre")), indicio));
        } else if (buscarPor.compareTo("titulo") == 0) {
            cq.where(cb.like(cb.upper(rt.get("idLibro").get("titulo")), indicio));
        } else if (buscarPor.compareTo("isbn") == 0) {
            cq.where(cb.like(cb.upper(rt.get("idLibro").get("categoria")), indicio));
        } else if (buscarPor.compareTo("subcategoria") == 0) {
            cq.where(cb.like(cb.upper(rt.get("idLibro").get("subcategoria")), indicio));
        } else if (buscarPor.compareTo("categoria") == 0) {
            cq.where(cb.like(cb.upper(rt.get("idLibro").get("categoria")), indicio));
        }
        return getEntityManager().createQuery(cq).getResultList();
    }
    
    
    /**
     * 
     * @param buscarPor: busca segun las la fecha actual, si es atrasada o si esta en espera
     * @return 
     */
    public List<T> findDate(String buscarPor) {
        javax.persistence.criteria.CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = cb.createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(rt);
        if (buscarPor.compareTo("atrasadas") == 0) {
            cq.where(cb.lessThan(rt.get("fechaRecogerLibros"), cb.currentDate()));
        } else if (buscarPor.compareTo("en espera") == 0) {
            cq.where(cb.greaterThanOrEqualTo(rt.get("fechaRecogerLibros"), cb.currentDate()));
        } else if (buscarPor.compareTo("hoy") == 0) {
            cq.where(cb.greaterThanOrEqualTo(rt.get("fechaRecogerLibros"), cb.currentDate()));
        } else if (buscarPor.compareTo("cualquier fecha") == 0) {}
        return getEntityManager().createQuery(cq).getResultList();
    }
    
    public List<T> findDate(String buscarPor, String userName) {
        if (userName.length() == 0)
            return findDate(buscarPor);
        javax.persistence.criteria.CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = cb.createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(rt);
        if (buscarPor.compareTo("atrasadas") == 0) {
            cq.where(
                    cb.and(
                        cb.like(rt.get("idUsuario").get("usuario"), userName),
                        cb.lessThan(rt.get("fechaRecogerLibros"), cb.currentDate())
                    )
            );
        } else if (buscarPor.compareTo("en espera") == 0) {
            cq.where(
                     cb.and(
                        cb.like(rt.get("idUsuario").get("usuario"), userName),
                        cb.greaterThanOrEqualTo(rt.get("fechaRecogerLibros"), cb.currentDate())
                    )
            );
        } else if (buscarPor.compareTo("cualquier fecha") == 0) {
            cq.where(
                        cb.like(rt.get("idUsuario").get("usuario"), userName)
            );
        } else if (buscarPor.compareTo("hoy") == 0) {
            cq.where(
                     cb.and(
                        cb.like(rt.get("idUsuario").get("usuario"), userName),
                        cb.equal(rt.get("fechaRecogerLibros"), cb.currentDate())
                    )
            );
        }
        return getEntityManager().createQuery(cq).getResultList();
    }
    
    public List<T> findAll() {
        javax.persistence.criteria.CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = cb.createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }
    
    public List<T> findDistinctLibro() {
        javax.persistence.criteria.CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = cb.createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(rt);
        cq.orderBy(cb.asc(rt.get("idAutor")));
        return getEntityManager().createQuery(cq).getResultList();
//        javax.persistence.criteria.CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
//        javax.persistence.criteria.CriteriaQuery cq = cb.createQuery();
//        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
//        Expression exp1 = cb.concat(rt.get("idAutor").get("nombre"), " ");
//        exp1 = cb.concat(exp1, rt.get("idAutor").get("apepat"));
//        Expression ex = cb.function("GROUP_CONCAT", String.class, exp1);
//        cq.select(cb.array(rt.get("idLibro"), ex));
//        cq.groupBy(rt.get("idLibro"));
//        List<T> l = new ArrayList();
//        for (Object o:getEntityManager().createQuery(cq).getResultList()) {
//            AutorLibro a = new AutorLibro();
//            a.setIdLibro((Libro) ((Object[]) o)[0]);
//            Autor au = new Autor();
//            au.setNombre((String) ((Object[]) o)[1]);
//            au.setApepat("");
//            a.setIdAutor(au);
//            l.add((T) a);
//        }   
//        return l;
    }

    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
}
