/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sesion;

import entidad.AutorLibro;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author susi
 */
@Stateless
public class AutorLibroFacade extends AbstractFacade<AutorLibro> {
    @PersistenceContext(unitName = "librosPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AutorLibroFacade() {
        super(AutorLibro.class);
    }
    
}
